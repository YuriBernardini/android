package com.example.esame;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

class Utilities {

    static final int ACTIVITY_ADD_TRIP = 1;

    static void insertFragment(AppCompatActivity activity, Fragment fragment, String tag) {
        activity.getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment_container, fragment, tag)
                .commit();
    }
}
