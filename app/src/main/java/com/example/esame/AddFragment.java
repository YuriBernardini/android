package com.example.esame;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import com.example.esame.ViewModel.AddItemViewModel;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import java.util.Objects;

public class AddFragment extends Fragment {

    private static final String LOG = "AddFragment";

    private AddItemViewModel model;

    private TextInputEditText descriptionTextInputEditText;
    private TextInputEditText dateTextInputEditText;
    private TextInputEditText amountTextInputEditText;
    private Spinner spinner;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        final Activity activity = getActivity();
        if(activity != null) {

            amountTextInputEditText = activity.findViewById(R.id.amountTextInputEditText);
            dateTextInputEditText = activity.findViewById(R.id.dateTextInputEditText);
            descriptionTextInputEditText = activity.findViewById(R.id.descriptionTextInputEditText);
            spinner = activity.findViewById(R.id.spinner);

            model = new ViewModelProvider((ViewModelStoreOwner) activity).get(AddItemViewModel.class);

            final TextInputLayout amountTextInputLayout = activity.findViewById(R.id.amountTextInputLayout);

            //using inputEditText to avoid empty
            amountTextInputEditText.setOnKeyListener((v, keyCode, event) -> {
                if(inputNotEmpty(amountTextInputEditText.getText())) {
                    amountTextInputLayout.setError(null); //clear error
                } else {
                    amountTextInputLayout.setError(getString(R.string.place_empty));
                }
                return false;
            });

            activity.findViewById(R.id.addButton).setOnClickListener(v -> {
                if(inputNotEmpty(amountTextInputEditText.getText())) {
                    //add item to the db using viewModel and Room
                    model.addMovement(new Movement(
                            Objects.requireNonNull(descriptionTextInputEditText.getText()).toString(),
                            Objects.requireNonNull(dateTextInputEditText.getText()).toString(),
                            spinner.getSelectedItem().toString(),
                            Double.parseDouble(amountTextInputEditText.getText().toString())));

                    activity.setResult(Activity.RESULT_OK);
                    activity.finish();

                } else {
                    amountTextInputLayout.setError(getString(R.string.place_empty));
                }
            });

        } else {
            Log.e(LOG, "Activity is null");
        }
    }

    private boolean inputNotEmpty(@Nullable Editable text) {
        return text != null && text.length() > 0;
    }
}
