package com.example.esame.Database;

import android.app.Application;
import androidx.lifecycle.LiveData;
import com.example.esame.Movement;
import java.util.List;

public class MovementItemRepository {

    private MovementDAO movementDAO;
    private LiveData<List<Movement>> item_list;

    public MovementItemRepository(Application application) {
        MovementDatabase db = MovementDatabase.getDatabase(application);
        movementDAO = db.movementDAO();
        item_list = movementDAO.getItems();
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    public void addMovement(final Movement movement) {
        MovementDatabase.databaseWriteExecutor.execute(() -> movementDAO.addMovement(movement));
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed
    public LiveData<List<Movement>> getItems() {
        return item_list;
    }

}
