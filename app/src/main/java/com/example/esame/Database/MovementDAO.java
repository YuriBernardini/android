package com.example.esame.Database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import com.example.esame.Movement;
import java.util.List;

@Dao
public interface MovementDAO {
    // The selected on conflict strategy ignores a new movement
    // if it's exactly the same as one already in the list.
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addMovement(Movement movement);

    @Transaction
    @Query("SELECT * from movimento ORDER BY id_movimento DESC")
    LiveData<List<Movement>> getItems();
}
