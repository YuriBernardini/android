package com.example.esame;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Class which represents every movement item with its information (amount, data, category, title)
 */
@Entity(tableName = "movimento")
public class Movement {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_movimento")
    private int id;

    @ColumnInfo(name = "titolo_movimento")
    private String title;

    @ColumnInfo(name = "data_movimento")
    private String date;

    @ColumnInfo(name = "categoria_movimento")
    private String category;

    @ColumnInfo(name = "importo_movimento")
    private double amount;

    //constructor
    public Movement(String title, String date, String category, double amount) {
        this.amount = amount;
        this.category = category;
        this.date = date;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public String getCategory() {
        return category;
    }

    public double getAmount() {
        return amount;
    }

}
