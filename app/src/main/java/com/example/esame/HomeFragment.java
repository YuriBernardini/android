package com.example.esame;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import com.example.esame.ViewModel.ListItemViewModel;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private static final String LOG = "Home-Fragment";

    private  TextView balanceTextView;

    //chart values
    private String[] cases = {"Entrate", "Uscite"};

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null.
     *
     * @param inflater, LayoutInflater object that can be used to inflate any views in the fragment,
     * @param container, If non-null, this is the parent view that the fragment's UI should be attached to.
     *                   The fragment should not add the view itself, but this can be used to
     *                   generate the LayoutParams of the view.
     * @param savedInstanceState, If non-null, this fragment is being re-constructed from a
     *                            previous saved state as given here.
     * @return the View for the fragment's UI, or null.
     */
    @SuppressLint("DefaultLocale")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        final FragmentActivity activity = getActivity();

        if(activity != null) {

            balanceTextView = view.findViewById(R.id.homeBalanceTextView);

            ListItemViewModel model = new ViewModelProvider(activity).get(ListItemViewModel.class);

            model.getItems().observe(getViewLifecycleOwner(), movement -> balanceTextView.
                    setText(getString(R.string.amount,  String.format("%.2f",getBalance(movement, view)))));

        } else {
            Log.e(LOG, "Activity is null");
        }

        //Inflate the layout for this fragment
        return view;
}

    private double getBalance(List<Movement> movement, View view) {

        //function for making chart
        setChart(movement, view);

        //calculate total balance
        double balance = 0d;
        for(Movement mov : movement) {
            balance = balance + mov.getAmount();
        }

        //change color textView
        /*if(balance < 0.00d) {
            balanceTextView.setTextColor(Color.rgb(204,0,0));
        } else {
            balanceTextView.setTextColor(Color.rgb(0,204,0));
        }*/

        return balance;
    }

    private void setChart(List<Movement> movement, View view) {

        double entrate = 0.00d;
        double uscite = 0.00d;

        //calculate entrate e uscite
        for(Movement mov : movement) {
            if(mov.getAmount() > 0){
                entrate = entrate + mov.getAmount();
            } else {
                uscite = uscite + mov.getAmount();
            }
        }
        //convert negative into positive
        if(uscite != 0) {
            uscite = uscite * -1;
        }

        //make float array for PieChart
        float[] value = {(float) entrate, (float) uscite};

        //Make CHART
        List<PieEntry> pieEntry = new ArrayList<>();
        for(int i = 0; i < value.length; i++) {
            pieEntry.add(new PieEntry( value[i], cases[i]));
        }
        PieDataSet dataSet = new PieDataSet(pieEntry, "");
        dataSet.setColors(Color.parseColor("#FF4CAF50"),Color.parseColor("#FA3D2F"));

        PieData data = new PieData(dataSet);
        data.setValueTextSize(20f);

        PieChart chart = view.findViewById(R.id.chart);
        chart.setData(data);

        chart.setDrawEntryLabels(false);
        chart.setDescription(null);
        chart.setDrawCenterText(false);
        chart.setUsePercentValues(false);
        chart.animateY(1000);
        chart.invalidate();

    }
}
