package com.example.esame;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import com.example.esame.ViewModel.ListItemViewModel;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Objects;

public class SettingsFragment extends Fragment  {

    private static final String LOG = "Setting-Fragment";

    private TextView balanceTextView;
    private TextView entrateTextView;
    private TextView usciteTextView;

    @SuppressLint("DefaultLocale")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        final FragmentActivity activity = getActivity();

        if(activity != null) {

            balanceTextView = view.findViewById(R.id.totaleSaldoTextView);
            entrateTextView = view.findViewById(R.id.entrateTextView);
            usciteTextView = view.findViewById(R.id.usciteTextView);

            ListItemViewModel model = new ViewModelProvider(activity).get(ListItemViewModel.class);

            view.findViewById(R.id.button).setOnClickListener(v -> exportCSV(activity, Objects.requireNonNull(model.getItems().getValue())));

            model.getItems().observe(getViewLifecycleOwner(), movement ->
            {
                double entrate = 0;
                double uscite = 0;
                double saldo;

                for(Movement mov : movement) {
                    if(mov.getAmount() > 0){
                        entrate = entrate + mov.getAmount();
                    } else {
                        uscite = uscite + mov.getAmount();
                    }
                }

                saldo = entrate + uscite;

                entrateTextView.setText(getString(R.string.totale_entrate,  String.format("%.2f",entrate)));
                usciteTextView.setText(getString(R.string.totale_uscite,  String.format("%.2f",uscite)));
                balanceTextView.setText(getString(R.string.amount,  String.format("%.2f",saldo)));

            });
        } else {
            Log.e(LOG, "Activity is null");
        }

        //Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void exportCSV( FragmentActivity activity, List<Movement> movements) {
        //generate data
        StringBuilder data = new StringBuilder();
        data.append("Importo,Categoria,Data");
        for(Movement mov : movements){
            data.append("\n").append(mov.getAmount()).append(",").append(mov.getCategory()).append(",").append(mov.getDate());
        }

        try {
            //save
            FileOutputStream out = activity.openFileOutput("data.csv", Context.MODE_PRIVATE);
            out.write((data.toString()).getBytes());
            out.close();
            //exporting
            Context context = activity.getApplicationContext();
            File filelocation = new File(activity.getFilesDir(), "data.csv");
            Uri path = FileProvider.getUriForFile(context, "com.example.esame.fileprovider", filelocation);
            Intent fileIntent = new Intent(Intent.ACTION_SEND);
            fileIntent.setType("text/csv");
            fileIntent.putExtra(Intent.EXTRA_SUBJECT, "Estratto Conto");
            fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            fileIntent.putExtra(Intent.EXTRA_STREAM, path);
            startActivity(Intent.createChooser(fileIntent, "Send mail"));
        } catch (Exception e) {
            Log.e(LOG, "ERROR");
            e.printStackTrace();
        }
    }
}
