package com.example.esame;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private static final String HOME_FRAGMENT_TAG = "homeFragment";

    ListFragment listFragment = new ListFragment();
    HomeFragment homeFragment = new HomeFragment();
    SettingsFragment settingsFragment = new SettingsFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //insert layoout in the activity
        setContentView(R.layout.activity_main);

        //the app is just started; never gone onStop()
       if(savedInstanceState == null) {
            Utilities.insertFragment(this, homeFragment, HOME_FRAGMENT_TAG);
       }

        //add action of FAB
        FloatingActionButton floatingActionButton = findViewById(R.id.fab_add);
        floatingActionButton.setOnClickListener(view -> startActivityForResult(new Intent(MainActivity.this, AddActivity.class),
                Utilities.ACTIVITY_ADD_TRIP));

        //set bottom nav menu
       BottomNavigationView bottomNav = findViewById(R.id.bottom_navigation);
       bottomNav.setOnNavigationItemSelectedListener(navListener);
    }

    /**
     * Method call after the   activity.setResult(RESULT_OK); e activity.finish(); in AddFragment.
     *
     * @param requestCode requestCode of the intent (ACTIVITY_ADD_TRIP in this case)
     * @param resultCode the result of the intent (RESULT_OK)
     * @param data the optional data in the intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult()");
        if(requestCode == Utilities.ACTIVITY_ADD_TRIP && resultCode == RESULT_OK) {
            Log.d(TAG, "RESULT_OK");
        }
     }

    //switch fragment by press bottom menu
    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            item -> {

                Fragment selectedFragment = null;

                switch (item.getItemId()) {
                    case R.id.homeMenu:
                        selectedFragment = homeFragment;
                        break;
                    case R.id.listMenu:
                        selectedFragment = listFragment;
                        break;
                    case R.id.settingMenu:
                        selectedFragment = settingsFragment;
                        break;
                }

                assert selectedFragment != null;
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectedFragment).commit();

                return true;
            };
}
