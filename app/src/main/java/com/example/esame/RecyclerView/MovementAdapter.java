package com.example.esame.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.esame.Movement;
import com.example.esame.R;
import java.util.ArrayList;
import java.util.List;

/**
 * Adapter linked to the RecyclerView of the listPage, that extends a custom ViewHolder
 */
public class MovementAdapter extends RecyclerView.Adapter<MovementViewHolder> {

    //my list
    private List<Movement> movementsList = new ArrayList<>();

    //listener attached to the onclick event for the item in the RecyclerView
    private OnItemListener listener;

    public MovementAdapter(OnItemListener listener) {
        this.listener = listener;
    }

    /**
     *
     * Called when RecyclerView needs a new RecyclerView.ViewHolder of the given type to represent an item.
     *
     * @param parent ViewGroup into which the new View will be added after it is bound to an adapter position.
     * @param viewType view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    @NonNull
    @Override
    public MovementViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movement_layout, parent, false);
        return new MovementViewHolder(layoutView, listener);
    }

    /**
     * Called by RecyclerView to display the data at the specified position.
     * This method should update the contents of the RecyclerView.ViewHolder.itemView to reflect
     * the item at the given position.
     *
     * @param holder ViewHolder which should be updated to represent the contents of the item at
     *               the given position in the data set.
     * @param position position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(@NonNull MovementViewHolder holder, int position) {
        final Movement currentMovement = movementsList.get(position);

        holder.amountTextView.setText(String.valueOf(currentMovement.getAmount()));
        holder.dateTextView.setText(currentMovement.getDate());
        holder.descriptionTextView.setText(currentMovement.getTitle());
        holder.categoryTextView.setText(currentMovement.getCategory());

    }

    @Override
    public int getItemCount() {
        return movementsList.size();
    }

    /**
     * Method called when a new item is added
     * @param newData the new list of items
     */
    public void setData(List<Movement> newData) {
        this.movementsList.clear();
        this.movementsList.addAll(newData);
        notifyDataSetChanged();
    }

}
