package com.example.esame.RecyclerView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import com.example.esame.R;

/**
 * A ViewHolder describes an item view and the metadata about its place within the RecyclerView.
 * Every item in the list has a listener for the onclick event
 */
public class MovementViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView amountTextView;
    TextView dateTextView;
    TextView descriptionTextView;
    TextView categoryTextView;

    private OnItemListener itemListener;

    MovementViewHolder(@NonNull View itemView, OnItemListener listener) {
        super(itemView);

        amountTextView = itemView.findViewById(R.id.homeBalanceTextView);
        dateTextView = itemView.findViewById(R.id.dateTextView);
        descriptionTextView = itemView.findViewById(R.id.descriptionTextView);
        categoryTextView = itemView.findViewById(R.id.categoryTextView);

        itemListener = listener;

        itemView.setOnClickListener(this);
    }

    /**
     * implementation of method in View.OnclickListener
     * @param v the view
     */    @Override
    public void onClick(View v) {
        itemListener.onItemClick(getAdapterPosition());
    }
}
