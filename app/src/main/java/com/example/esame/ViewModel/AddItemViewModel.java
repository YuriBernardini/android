package com.example.esame.ViewModel;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import com.example.esame.Database.MovementItemRepository;
import com.example.esame.Movement;

/**
 * The ViewModel class is designed to store and manage UI-related data in a lifecycle conscious way.
 * The ViewModel class allows data to survive configuration changes such as screen rotations.
 *
 * The data stored by ViewModel are not for long term. (Until activity is destroyed)
 *
 * This ViewModel is linked to the addFragment, so it has only the addItem method
 */
public class AddItemViewModel extends AndroidViewModel {

    private MovementItemRepository repository;

    public AddItemViewModel(@NonNull Application application) {
        super(application);
        repository = new MovementItemRepository(application);
    }

    public void addMovement(Movement item) {
        repository.addMovement(item);
    }
}
