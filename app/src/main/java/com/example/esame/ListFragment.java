package com.example.esame;

import android.os.Bundle;
import android.transition.Slide;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.esame.RecyclerView.MovementAdapter;
import com.example.esame.RecyclerView.OnItemListener;
import com.example.esame.ViewModel.ListItemViewModel;
import java.util.Objects;

public class ListFragment extends Fragment implements OnItemListener {

    private static final String LOG = "List-Fragment";

    private MovementAdapter adapter;

    private RecyclerView recyclerView;

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null.
     *
     * @param inflater, LayoutInflater object that can be used to inflate any views in the fragment,
     * @param container, If non-null, this is the parent view that the fragment's UI should be attached to.
     *                   The fragment should not add the view itself, but this can be used to
     *                   generate the LayoutParams of the view.
     * @param savedInstanceState, If non-null, this fragment is being re-constructed from a
     *                            previous saved state as given here.
     * @return the View for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_list, container, false);
        final FragmentActivity activity = getActivity();

        if(activity != null){

            setRecyclerView(view);

            ListItemViewModel model = new ViewModelProvider(activity).get(ListItemViewModel.class);

            model.getItems().observe(getViewLifecycleOwner(), movements -> adapter.setData(movements));

        } else {
            Log.e(LOG, "Activity is null");
        }

        return view;
    }

    /**
     * Called to do initial creation of a fragment.
     * Note that this can be called while the fragment's activity is still in the process of being created.
     * @param savedInstanceState  If the fragment is being re-created from a previous saved state, this is the state.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Method to set the RecyclerView and the relative adapter
     * @param view the current view
     */
    private void setRecyclerView(final View view) {

        recyclerView = view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);

        //define adapter
        final OnItemListener listener = this;
        adapter = new MovementAdapter(listener);
        recyclerView.setAdapter(adapter);

    }

    /**
     * Method called when the user click a card
     * @param position position of the item clicked
     */
    @Override
    public void onItemClick(int position) {
        final View view = Objects.requireNonNull(recyclerView.findViewHolderForAdapterPosition(position)).itemView;
        final CardView cardView = view.findViewById(R.id.single_movement);
        final View expandibleView = cardView.findViewById(R.id.expandableView);

        //trasition to show hidden layout
        TransitionManager.beginDelayedTransition(cardView, new Slide());
        expandibleView.setVisibility(expandibleView.getVisibility() == View.GONE ? View.VISIBLE : View.GONE);

    }
}
